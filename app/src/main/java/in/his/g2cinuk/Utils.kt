package `in`.his.g2cinuk

import `in`.his.g2cinuk.apiinterface.ApiService
import `in`.his.g2cinuk.apiinterface.RetroClient
import `in`.his.g2cinuk.dataModel.*
import android.content.Context
import android.util.Log
import android.widget.Toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Utils {

    fun getPlan(c: Context) {
        try {
            val call = apiService.PRODUCTION_PLAN_RESPONSE_CALL()
            call.enqueue(object : Callback<ProductionPlanResponse> {
                override fun onFailure(call: Call<ProductionPlanResponse>, t: Throwable) {
                    Toast.makeText(
                        c,
                        "Please Check Network Connection First", Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onResponse(
                    call: Call<ProductionPlanResponse>,
                    response: Response<ProductionPlanResponse>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            if (response.body()!!.proplan!!.isNotEmpty()) {
                                proPlan = emptyList()
                                proPlan = response.body()!!.proplan!!
                                lineNos.clear()
                                for (i in proPlan) {
                                    lineNos.add("${i!!.lineNo!!}:${i.sku}:${i.batchNo}")
                                }
                            } else {
                                Toast.makeText(
                                    c,
                                    "There is no working plan available,Please contact to administrator",
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }
                    }
                }
            })
        } catch (e: Exception) {
        }
    }

    fun getSku(c: Context) {
        try {
            val call = apiService.SKU_LIST_BARCODE_RESPONSE_CALL()
            call.enqueue(object : Callback<ResponseAllSku> {
                override fun onResponse(
                    call: Call<ResponseAllSku>,
                    response: Response<ResponseAllSku>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        if (response.body()!!.skuData!!.isNotEmpty()) {
                            skusList.clear()
                            skuList = response.body()!!.skuData!!
                            skusList.clear()
                            for (i in skuList) {
                                skusList.add(i?.sku!!)
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<ResponseAllSku>, t: Throwable) {
                    Toast.makeText(
                        c, "Please Check Network Connection First",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
        } catch (e: Exception) {
        }
    }

    fun getBay() {
        try {
            val call = apiService.BAY_RESPONSE_BARCODE_CALL()
            call.enqueue(object : Callback<ResponseAllBayDetails> {
                override fun onResponse(
                    call: Call<ResponseAllBayDetails>,
                    response: Response<ResponseAllBayDetails>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        if (response.body()!!.bayDetails!!.isNotEmpty()) {
                            bayList = emptyList()
                            bayList = response.body()!!.bayDetails!!
                            baysList.clear()
                            for (i in bayList) {
                                baysList.add(i!!.bay!!)
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<ResponseAllBayDetails>, t: Throwable) {

                }

            })
        } catch (e: Exception) {
        }
    }

    fun getAllData() {
        try {
            val call = apiService.PRODUCTION_ALL_BARCODE_RESPONSE_CALL()
            call.enqueue(object : Callback<ResponseAllData> {
                override fun onFailure(call: Call<ResponseAllData>, t: Throwable) {

                }

                override fun onResponse(
                    call: Call<ResponseAllData>,
                    response: Response<ResponseAllData>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        when {
                            response.body()!!.bayWithProduction.isNullOrEmpty() -> {
                            }
                            else -> {
                                allData = emptyList()
                                allData = response.body()!!.bayWithProduction!!
                                Log.d("ListSize", allData.size.toString())
                            }
                        }
                    }
                }
            })
        } catch (e: Exception) {
        }
    }

    companion object {
        var allData: List<BayWithProductionItem?> = ArrayList<BayWithProductionItem>()
        var proPlan: List<ProplanItem?> = ArrayList<ProplanItem>()
        var skuList: List<SkuItem?> = ArrayList<SkuItem>()
        var bayList: List<BayItemDetails?> = ArrayList<BayItemDetails>()
        var skusList = ArrayList<String>()
        var baysList = ArrayList<String>()
        var lineNos = ArrayList<String>()
        private var apiService: ApiService = RetroClient.getClient().create(ApiService::class.java)
        var bayBarcode = ""
        var bayName = ""
        var pBarcode = ""
    }
}