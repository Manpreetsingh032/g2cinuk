package `in`.his.g2cinuk

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.uk2.*

class HeronStDepot : Fragment() {
    private lateinit var handleClick: HandleClick

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.uk2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.handleClick = ClickObjHandle.instance().handleClick

        ViewListener.instance().addBayView(object : SetBayView {
            override fun changeView() {
                findEmptyBay()
            }
        })

        her_h4?.setOnClickListener {
            performClick("HER H4")

        }
        her_h3?.setOnClickListener {
            performClick("HER H3")

        }
        her_h2?.setOnClickListener {
            performClick("HER H2")

        }
        her_h1?.setOnClickListener {
            performClick("HER H1")

        }
        her_g4?.setOnClickListener {
            performClick("HER G4")

        }
        her_g3?.setOnClickListener {
            performClick("HER G3")

        }
        her_g2?.setOnClickListener {
            performClick("HER G2")

        }
        her_g1?.setOnClickListener {
            performClick("HER G1")

        }
        her_f6?.setOnClickListener {
            performClick("HER F6")

        }
        her_f5?.setOnClickListener {
            performClick("HER F5")

        }
        her_f4?.setOnClickListener {
            performClick("HER F4")

        }
        her_f3?.setOnClickListener {
            performClick("HER F3")

        }
        her_f2?.setOnClickListener {
            performClick("HER F2")

        }
        her_f1?.setOnClickListener {
            performClick("HER F1")

        }
        her_e1?.setOnClickListener {
            performClick("HER E1")

        }
        her_e2?.setOnClickListener {
            performClick("HER E2")

        }
        her_e3?.setOnClickListener {
            performClick("HER E3")

        }
        her_d5d6?.setOnClickListener {
            performClick("HER D5 + D6")

        }
        her_d4?.setOnClickListener {
            performClick("HER D4")

        }
        her_d3?.setOnClickListener {
            performClick("HER D3")

        }
        her_d2?.setOnClickListener {
            performClick("HER D2")

        }
        her_d1?.setOnClickListener {
            performClick("HER D1")

        }
        her_c5c6?.setOnClickListener {
            performClick("HER C5 + C6")

        }
        her_c4?.setOnClickListener {
            performClick("HER C4")

        }
        her_c3?.setOnClickListener {
            performClick("HER C3")

        }
        her_c2?.setOnClickListener {
            performClick("HER C2")

        }
        her_c1?.setOnClickListener {
            performClick("HER C1")

        }
        her_b1b2?.setOnClickListener {
            performClick("HER B1 + B2")

        }
        her_a4?.setOnClickListener {
            performClick("HER A4")

        }
        her_a3?.setOnClickListener {
            performClick("HER A3")

        }
        her_a2?.setOnClickListener {
            performClick("HER A2")

        }
        her_a1?.setOnClickListener {
            performClick("HER A1")

        }
        zone_2a?.setOnClickListener {
            performClick("Postal + Docs Service Desk Zone 2A")

        }
        zone_1z2z?.setOnClickListener {
            performClick("CUSTOMS AREA ZONE 1Z+2Z")

        }
        zone_1a1d?.setOnClickListener {
            performClick("CUSTOMS AREA ZONE 1A+1D")

        }
        zone_3b?.setOnClickListener {
            performClick("DAMAGED GOOD BAY ZONE 3B")

        }
        zone_4a?.setOnClickListener {
            performClick("ZONE 4A")

        }
        frozen_9d?.setOnClickListener {
            performClick("FROZEN STORAGE 9D")

        }
        zone_7a8a?.setOnClickListener {
            performClick("STORAGE BIN AREA ASK FOR LOCATION NAMES ZONE 7A+8A")

        }
        lleft_path?.setOnClickListener {
            performClick("LEFT-PATH")

        }
        rright_path?.setOnClickListener {
            performClick("RIGHT-PATH")

        }
        h4_path?.setOnClickListener {
            performClick("H4-PATH")

        }
        g4_path?.setOnClickListener {
            performClick("G4-PATH")

        }
        f1f4_path?.setOnClickListener {
            performClick("F1F4-PATH")

        }
        c4d4_path?.setOnClickListener {
            performClick("C4D5-PATH")

        }
        d5d6_path?.setOnClickListener {
            performClick("D5D6-PATH")

        }
        customer_area_path?.setOnClickListener {
            performClick("CUSTOMER-AREA-PATH")

        }
        e1_path?.setOnClickListener {
            performClick("E1-PATH")

        }
        e1e2_path?.setOnClickListener {
            performClick("E1E2-PATH")

        }
        a4c4_path?.setOnClickListener {
            performClick("A4C4-PATH")

        }
        zone_4a_path?.setOnClickListener {
            performClick("ZONE-4A-PATH")

        }
        z4a_storage_path?.setOnClickListener {
            performClick("Z4A-STORAGE-PATH")

        }
        zone_7a8a_path?.setOnClickListener {
            performClick("ZONE-7A8A-PATH")

        }
        top?.setOnClickListener {
            performClick("TOP-PATH")

        }
        bottom?.setOnClickListener {
            performClick("BOTTOM-PATH")

        }
        lb_500_path?.setOnClickListener {
            performClick("LB-500-PATH")

        }
        zone_3b_path?.setOnClickListener {
            performClick("ZONE-3B-PATH")
        }
        zone_2a_path?.setOnClickListener {
            performClick("ZONE-2A-PATH")
        }
        z2a3b_path?.setOnClickListener {
            performClick("Z2A3B-PATH")
        }
        gi_path?.setOnClickListener {
            performClick("GOOD-INWARD-PATH")
        }
        go_path?.setOnClickListener {
            performClick("GOOD-OUTWARD-PATH")
        }
        gi_desk_path?.setOnClickListener {
            performClick("GI-DESK-PATH")
        }
        go_desk_path?.setOnClickListener {
            performClick("GO-DESK-PATH")
        }

        findEmptyBay()
    }

    private fun performClick(name: String) {
        val status = Search().bayMaxCapacity(name)!!
            .toInt() == Search().getBayPalletQty(name)
        if (this::handleClick.isInitialized) {
            handleClick.clickBay(name, status)

        }
    }

    private fun findEmptyBay() {

        setBack("HER H4", her_h4)

        setBack("HER H3", her_h3)

        setBack("HER H2", her_h2)

        setBack("HER H1", her_h1)

        setBack("HER G4", her_g4)

        setBack("HER G3", her_g4)

        setBack("HER G2", her_g1)

        setBack("HER G1", her_g1)

        setBack("HER F6", her_f6)

        setBack("HER F5", her_f5)

        setBack("HER F4", her_f4)

        setBack("HER F3", her_f3)

        setBack("HER F2", her_f2)

        setBack("HER F1", her_f1)

        setBack("HER D5 + D6", her_d5d6)

        setBack("HER D4", her_d4)

        setBack("her D3", her_d3)

        setBack("HER D2", her_d2)

        setBack("HER D1", her_d1)

        setBack("HER C5 + C6", her_c5c6)

        setBack("her C4", her_c4)

        setBack("HER C3", her_c3)

        setBack("HER C2", her_c2)

        setBack("HER C1", her_c1)

        setBack("HER B1 + B2", her_b1b2)

        setBack("HER A4", her_a4)

        setBack("HER A3", her_a3)

        setBack("HER A2", her_a2)

        setBack("HER A1", her_a1)



    }

    private fun setBack(name: String, layout: LinearLayout?) {
        try {
            val list = Search().searchFromBay(name)
            if (list[0].sku.equals("empty", true)) {
                layout?.background =
                    ResourcesCompat.getDrawable(resources, R.drawable.empty_back, null)
            } else {
                layout?.background =
                    ResourcesCompat.getDrawable(resources, R.drawable.main_back, null)
            }
        } catch (e: Exception) {
            Log.d("TAG", "setBack: $e")
        }
    }


    companion object {
        fun newInstance() =
            HeronStDepot()
    }
}