package `in`.his.g2cinuk

import `in`.his.g2cinuk.dataModel.BayWithProductionItem
import `in`.his.g2cinuk.dataModel.SkuDataItem

class Search {
    fun searchFromBay(bayName: String?): ArrayList<BayWithProductionItem> {
        val searchList = ArrayList<BayWithProductionItem>()
        try {
            for (store in Utils.allData) {
                if (store!!.bay.equals(bayName, true)) {
                    searchList.add(store)
                }
            }
        } catch (e: Exception) {
        }
        return searchList
    }

    fun bayMaxCapacity(bayName: String?): Double? {
        try {
            Utils.bayList.forEach {
                if (it!!.bay == bayName?.trim()) {
                    return it.capacity
                }
            }
        } catch (e: Exception) {

        }
        return 0.0
    }

    fun getBayPalletQty(bayName: String?): Int {
        val searchList = ArrayList<BayWithProductionItem>()
        val currentSkuPalletQtyList = ArrayList<SkuDataItem>()
        var totalPallets = 0
        try {
            for (store in Utils.allData) {
                if (store!!.bay.equals(bayName!!.trim(), true)) {
                    searchList.add(store)
                }
            }

            val uniqueSkuList: List<BayWithProductionItem> = searchList.distinctBy { it.sku }
            uniqueSkuList.forEach {
                for (i in Utils.skuList) {
                    try {
                        if (i!!.sku == it.sku) {
                            currentSkuPalletQtyList.add(
                                SkuDataItem(
                                    i.palletWeight!!.toDouble(),
                                    1,
                                    i.sku!!.toString(),
                                    i.casesOfPallets!!.toDouble(),
                                    null
                                )
                            )
                        }
                    } catch (e: Exception) {
                    }
                }
            }

            uniqueSkuList.forEach { skuName ->
                var totalQty = 0
                val singleSkuPalletQty =
                    currentSkuPalletQtyList.single { it.sku == skuName.sku }.casesOfPallets
                searchList.forEach { bayItem ->
                    if (bayItem.sku.equals(skuName.sku?.trim(), true)) {
                        totalQty += bayItem.qty!!
                    }
                }
                var a = totalQty.toDouble() / singleSkuPalletQty!!
                if (a > a.toInt()) {
                    a++
                }
                totalPallets += a.toInt()
            }
            print(totalPallets)
            return totalPallets
        } catch (e: Exception) {
        }

        return totalPallets
    }

    fun searchFromAllExceptEmpty(empty: String?): ArrayList<BayWithProductionItem> {
        val searchList = ArrayList<BayWithProductionItem>()
        try {
            for (store in Utils.allData) {
                if (!store!!.sku.equals(empty, true)) {
                    searchList.add(store)
                }
            }
        } catch (e: Exception) {
        }
        return searchList
    }

    fun searchFromAll(keySearch: String): ArrayList<BayWithProductionItem> {
        val searchList = ArrayList<BayWithProductionItem>()
        try {
            for (store in Utils.allData) {
                if (store!!.sku!!.contains(keySearch, true)
                    || store.expiry!!.toString().contains(keySearch, true)
                    || store.bay!!.contains(keySearch, true)
                ) {

                    searchList.add(store)
                }
            }
        } catch (e: Exception) {
        }
        return searchList
    }

    fun searchFromBlock(block: String): ArrayList<BayWithProductionItem> {
        val searchList = ArrayList<BayWithProductionItem>()
        try {
            for (store in Utils.allData) {
                if (store!!.bay!!.contains("" + block, true)) {
                    searchList.add(store)
                }
            }
        } catch (e: Exception) {
        }
        return searchList
    }

    fun searchFromBatch(batchNo: String): ArrayList<BayWithProductionItem> {
        val searchList = ArrayList<BayWithProductionItem>()
        try {
            for (store in Utils.allData) {
                if (store!!.expiry.toString().contains(batchNo)) {
                    searchList.add(store)
                }
            }
        } catch (e: Exception) {
        }
        return searchList
    }

    fun getSkuItem(sku: String?): ArrayList<BayWithProductionItem> {
        val searchList = ArrayList<BayWithProductionItem>()
        try {
            for (store in Utils.allData) {
                if (store!!.sku.toString().contentEquals(sku!!)) {
                    searchList.add(store)
                }
            }
        } catch (e: Exception) {
        }
        return searchList
    }


}