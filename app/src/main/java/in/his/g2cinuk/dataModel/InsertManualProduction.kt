package `in`.his.g2cinuk.dataModel

import com.google.gson.annotations.SerializedName

data class InsertManualProduction(

	/*private String sku;
    private String expiry;
    private int qty;
    private String status;
    private  String barcode;
    private String p_barcode;*/

	@field:SerializedName("qty")
	val qty: Int? = 0,

	@field:SerializedName("expiry")
	val expiry: String? = null,

	@field:SerializedName("sku")
	val sku: String? = null,

	@field:SerializedName("barcode")
	val barcode: String? = null,

	@field:SerializedName("p_barcode")
	val pBarcode: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)
