package `in`.his.g2cinuk.dataModel

import com.google.gson.annotations.SerializedName

data class ResponseGoodStatus(

	@field:SerializedName("message")
	val message: String? = null
)
