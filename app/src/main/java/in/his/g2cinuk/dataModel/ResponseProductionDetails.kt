package `in`.his.g2cinuk.dataModel

import com.google.gson.annotations.SerializedName

data class ResponseProductionDetails(

	@field:SerializedName("productionDetails")
	val productionDetails: List<ProductionDetailsItem?>? = null
)

data class ProductionDetailsItem(

	@field:SerializedName("qty")
	val qty: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("expiry")
	val expiry: String? = null,

	@field:SerializedName("sku")
	val sku: String? = null,

	@field:SerializedName("barcode")
	val barcode: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)
