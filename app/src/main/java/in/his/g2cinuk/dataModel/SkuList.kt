package `in`.his.g2cinuk.dataModel

import com.google.gson.annotations.SerializedName

data class SkuList(

	@field:SerializedName("SkuData")
	val skuData: List<SkuDataItem?>? = null
)

data class SkuDataItem(

	@field:SerializedName("pallet_weight")
	val palletWeight: Double? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("sku")
	val sku: String? = null,

	@field:SerializedName("cases_of_pallets")
	val casesOfPallets: Double? = null,

	@field:SerializedName("p_barcode")
	val pBarcode: Any? = null
)
