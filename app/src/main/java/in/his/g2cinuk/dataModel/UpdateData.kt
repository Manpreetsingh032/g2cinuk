package `in`.his.g2cinuk.dataModel

import com.google.gson.annotations.SerializedName

data class UpdateData(

	@field:SerializedName("hsn")
	val hsn: String? = null,

	@field:SerializedName("carton_gross_weight")
	val cartonGrossWeight: String? = null,

	@field:SerializedName("no_of_pcs")
	val noOfPcs: String? = null,

	@field:SerializedName("name_of_item")
	val nameOfItem: String? = null,

	@field:SerializedName("packaging")
	val packaging: String? = null,

	@field:SerializedName("barcode")
	val barcode: String? = null,

	@field:SerializedName("user_name")
	val user_name: String? = null,

	@field:SerializedName("per_pcs_weight")
	val perPcsWeight: String? = null
)
