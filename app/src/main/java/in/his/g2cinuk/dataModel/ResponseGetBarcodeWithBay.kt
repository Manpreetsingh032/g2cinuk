package `in`.his.g2cinuk.dataModel

data class ResponseGetBarcodeWithBay(
	val barcodeList: List<BarcodeListItem?>? = null
)

data class BarcodeListItem(
	val id: Int? = null,
	val bay: String? = null,
	val barcode: String? = null,
	val capacity: Double? = null
)

