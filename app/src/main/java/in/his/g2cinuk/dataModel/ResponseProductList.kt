package `in`.his.g2cinuk.dataModel

import com.google.gson.annotations.SerializedName

data class ResponseProductList(

    @field:SerializedName("productDetails")
    val productDetails: List<ProductDetailsItem?>? = null
)

/*{"productDetails":[{"id":2,"barcode":"092399613619",
"num_pcs":56,"name_of_item":"KACHHA AAM JAR b",
"per_pcs_weight":2.5,"packaging":12,"carton_gross_weight":9.5,
"hsn":"741852963"}]}*/

data class ProductDetailsItem(

    @field:SerializedName("hsn")
    val hsn: Any? = null,

    @field:SerializedName("carton_gross_weight")
    val cartonGrossWeight: Double? = null,

    @field:SerializedName("name_of_item")
    val nameOfItem: String? = null,

    @field:SerializedName("packaging")
    val packaging: Int? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("barcode")
    val barcode: String? = null,

    @field:SerializedName("num_pcs")
    val numPcs: String? = null,

    @field:SerializedName("per_pcs_weight")
    val perPcsWeight: Double? = null,

    @field:SerializedName("user_name")
    val userName: String? = null
)
