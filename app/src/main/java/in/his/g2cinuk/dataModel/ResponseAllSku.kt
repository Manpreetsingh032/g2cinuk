package `in`.his.g2cinuk.dataModel

import com.google.gson.annotations.SerializedName

data class ResponseAllSku(

    @field:SerializedName("SkuData")
    val skuData: List<SkuItem?>? = null
)

data class SkuItem(

    @field:SerializedName("date")
    val date: Any? = null,

    @field:SerializedName("pallet_weight")
    val palletWeight: String? = null,

    @field:SerializedName("expiry_date")
    val expiryDate: Any? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("sku")
    val sku: String? = null,

    @field:SerializedName("cases_of_pallets")
    val casesOfPallets: String? = null,

    @field:SerializedName("p_barcode")
    val pBarcode: Any? = null
)
