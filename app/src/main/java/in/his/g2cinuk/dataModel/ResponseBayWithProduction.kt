package `in`.his.g2cinuk.dataModel

import com.google.gson.annotations.SerializedName

data class ResponseBayWithProduction(

	@field:SerializedName("bayWithProduction")
	val bayWithProduction: BayWithProduction? = null
)

data class BayWithProduction(

	@field:SerializedName("qty")
	val qty: Int? = null,

	@field:SerializedName("expiry")
	val expiry: String? = null,

	@field:SerializedName("bay")
	val bay: String? = null,

	@field:SerializedName("sku")
	val sku: String? = null,

	@field:SerializedName("barcode")
	val barcode: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)
