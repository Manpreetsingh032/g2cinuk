package in.his.g2cinuk;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

import in.his.g2cinuk.dataModel.BayWithProductionItem;

public class SearchAdapter extends
        RecyclerView.Adapter<SearchAdapter.MyViewHolder> {

    private ArrayList<BayWithProductionItem> list = new ArrayList<>();

    /**
     * View holder class
     */
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView bayno, batchNo, sku, qty, Number;

        /*
        *   holder.itemView.item_number_count.text = (position + 1).toString()
        holder.itemView.item_data_of_bay.text = list[position].bayNo
        holder.itemView.item_data_of_sku.text = list[position].sku
        holder.itemView.item_data_of_batch_no.text = list[position].batchNo.toString()
        holder.itemView.item_data_of_qty.text = list[position].qty.toString()
 */
        public MyViewHolder(View view) {
            super(view);
            bayno = view.findViewById(R.id.item_data_of_bay);
            sku = view.findViewById(R.id.item_data_of_sku);
            batchNo = view.findViewById(R.id.item_data_of_batch_no);
            qty = view.findViewById(R.id.item_data_of_qty);
            Number = view.findViewById(R.id.item_number_count);
            Number.setSelected(true);
            bayno.setSelected(true);
            sku.setSelected(true);
            batchNo.setSelected(true);
            qty.setSelected(true);
        }
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        //if(!list.get(position).getSku().equalsIgnoreCase("empty")){

        holder.Number.setText(String.valueOf(position + 1));
        holder.bayno.setText(list.get(position).getBay());
        holder.sku.setText(list.get(position).getSku());
        holder.batchNo.setText(String.valueOf(list.get(position).getExpiry()));
        holder.qty.setText(String.valueOf(list.get(position).getQty()));


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_bay_values, parent, false);
        return new MyViewHolder(v);
    }

    public void setList(ArrayList<BayWithProductionItem> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void sortFifo() {
        Collections.sort(list, (productionItem, t1) -> Objects.requireNonNull(
                productionItem.getExpiry()).compareTo(
                Objects.requireNonNull(t1.getExpiry())));
        setList(list);
    }

    public void sortLifo() {
        Collections.sort(list, (productionItem, t1) -> Objects.requireNonNull(
                t1.getExpiry()).compareTo(
                Objects.requireNonNull(productionItem.getExpiry())));
        setList(list);
    }
}
