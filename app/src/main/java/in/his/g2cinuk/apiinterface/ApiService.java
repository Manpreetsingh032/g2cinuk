package in.his.g2cinuk.apiinterface;

import in.his.g2cinuk.dataModel.BayResponse;
import in.his.g2cinuk.dataModel.InsertManualProduction;
import in.his.g2cinuk.dataModel.InsertModel;
import in.his.g2cinuk.dataModel.Password;
import in.his.g2cinuk.dataModel.ProductionAllResponse;
import in.his.g2cinuk.dataModel.ProductionPlanResponse;
import in.his.g2cinuk.dataModel.ResponseAllBayDetails;
import in.his.g2cinuk.dataModel.ResponseAllData;
import in.his.g2cinuk.dataModel.ResponseAllSku;
import in.his.g2cinuk.dataModel.ResponseGetBarcodeWithBay;
import in.his.g2cinuk.dataModel.ResponseInsertProduction;
import in.his.g2cinuk.dataModel.ResponseProductList;
import in.his.g2cinuk.dataModel.ResponseProductionDetails;
import in.his.g2cinuk.dataModel.SkuItem;
import in.his.g2cinuk.dataModel.SkuList;
import in.his.g2cinuk.dataModel.UpdateData;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {
    String user_name = "warehouse1";
    /*http://192.168.43.106:8082/login*/

    @GET("getLogin/")
    Call<Password> PASSWORD_CALL(@Query("user_name") String user_name,
                                 @Query("password") String password);

    @GET("getSkuListData/")
    Call<SkuList> SKU_LIST_RESPONSE_CALL();

    @GET("getAllProductionData/")
    Call<ProductionAllResponse> PRODUCTION_ALL_RESPONSE_CALL();

    @POST("insertProduction/")
    Call<Password> INSERT_RESPONSE_CALL(@Query("line_no") String line_no, @Body InsertModel insertModel);

    @GET("getTodayProductionPlan/")
    Call<ProductionPlanResponse> PRODUCTION_PLAN_RESPONSE_CALL();

    @GET("getBayList/")
    Call<BayResponse> BAY_RESPONSE_CALL();


    @GET("getProductBarcodeList/")
    Call<ResponseProductList> PRODUCT_LIST_CALL(@Query("barcode") String barcode,
                                                @Query("user_name") String user_name);

    @POST("insertAddProduct/")
    Call<ResponseInsertProduction> PRODUCT_INSERT_PRODUCTION(@Body UpdateData updateData);

    @GET("checkPathPassword/")
    Call<Password> PATH_PASSWORD_CALL(@Query("password") String password);

    @GET("/api/getBarcodeWithBay")
    Call<ResponseGetBarcodeWithBay> BARCODE_WITH_BAY(@Query("barcode") String barcode);

    @GET("/api/getproductionDetails")
    Call<ResponseProductionDetails> PRODUCTION_DETAILS(@Query("barcode") String barcode);


    @GET("getBayList/")
    Call<ResponseAllBayDetails> BAY_RESPONSE_BARCODE_CALL();

    @GET("getSkuListData/")
    Call<ResponseAllSku> SKU_LIST_BARCODE_RESPONSE_CALL();

    @GET("getBayWithProduction/")
    Call<ResponseAllData> PRODUCTION_ALL_BARCODE_RESPONSE_CALL();

    @POST("insertManualProduction/")
    Call<ResponseInsertProduction> INSERT_MANUAL_PRODUCTION(@Body InsertManualProduction insertManualProduction);

    @POST("updateSku/")
    Call<Password> SKU_ITEM_CALL(@Body SkuItem skuItem);

    @GET("getSkuListWithBarcode/")
    Call<ResponseAllSku> PRODUCT_DETAILS(@Query("p_barcode") String barcode);

}
