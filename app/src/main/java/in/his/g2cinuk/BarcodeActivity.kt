package `in`.his.g2cinuk

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.View
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import kotlinx.android.synthetic.main.barcode_scan.*
import kotlinx.android.synthetic.main.manual_barcode.*
import java.io.IOException


class BarcodeActivity : AppCompatActivity() {
    private lateinit var barcodeDetector: BarcodeDetector
    var c = this@BarcodeActivity
    lateinit var cameraSource: CameraSource
    var barcodeData = ""
    lateinit var surfaceView: SurfaceView
    private lateinit var manualBarcodeDialog: Dialog
    private val animation: Animation by lazy {
        AnimationUtils.loadAnimation(
            c,
            R.anim.line_anim
        )
    }
    lateinit var bar: View
    private var cameraStatus = false
    var clickData: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.barcode_scan)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        clickData = intent.extras?.get("ScanBarcode")?.toString()
        surfaceView = findViewById(R.id.surface_view)
        bar = findViewById(R.id.bar)
        animation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {
                bar.visibility = View.VISIBLE
            }

            override fun onAnimationEnd(animation: Animation) {
                bar.visibility = View.GONE
            }

            override fun onAnimationRepeat(animation: Animation) {}
        })

        manualBarcodeDialog = Dialog(c)
        with(manualBarcodeDialog) {
            setContentView(R.layout.manual_barcode)
            setCancelable(true)
            setCanceledOnTouchOutside(true)

            cancel_manual_barcode.setOnClickListener {
                dismiss()
            }

            submit_manual_barcode.setOnClickListener {
                if (manual_barcode.text.isNotEmpty()) {
                    barcodeData = manual_barcode.text.toString()
                    val intent = Intent(
                        c,
                        MainActivity::class.java
                    )
                    intent.putExtra("barcodeData", barcodeData)
                    intent.putExtra("ScanBarcode", clickData)

                    barcodeDetector.release()
                    startActivity(intent)
                    runOnUiThread {
                        cameraSource.stop()
                    }
                    dismiss()
                    finish()
                } else {
                    Toast.makeText(
                        c,
                        "Please enter barcode first",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            setOnCancelListener {
                c.onResume()
            }

            setOnDismissListener {
                c.onResume()
            }
        }


        close.setOnClickListener {
            bar.clearAnimation()
            onPause()
            cameraStatus = true
            manualBarcodeDialog.show()
        }


    }

    override fun onResume() {
        super.onResume()
        barcodeDetector = BarcodeDetector.Builder(applicationContext)
            .setBarcodeFormats(Barcode.ALL_FORMATS)
            .build()

        cameraSource = CameraSource.Builder(applicationContext, barcodeDetector)
            .setFacing(CameraSource.CAMERA_FACING_BACK)
            .setRequestedPreviewSize(1280, 1024)
            .setAutoFocusEnabled(true)
            .setRequestedFps(64.0f)
            .build()

        if (cameraStatus) {
            try {
                if (ActivityCompat.checkSelfPermission(
                        applicationContext,
                        Manifest.permission.CAMERA
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    ActivityCompat.requestPermissions(
                        c,
                        arrayOf(Manifest.permission.CAMERA),
                        1001
                    )
                    return
                }
                cameraSource.start(surfaceView.holder)

                bar.startAnimation(animation)

            } catch (e: IOException) {
                e.printStackTrace()
            }
        }


        surfaceView.holder.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceCreated(holder: SurfaceHolder) {
                try {
                    if (ActivityCompat.checkSelfPermission(
                            applicationContext,
                            Manifest.permission.CAMERA
                        ) != PackageManager.PERMISSION_GRANTED
                    ) {
                        ActivityCompat.requestPermissions(
                            c,
                            arrayOf(Manifest.permission.CAMERA),
                            1001
                        )
                        return
                    }
                    cameraSource.start(surfaceView.holder)

                    bar.startAnimation(animation)

                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }

            override fun surfaceChanged(
                holder: SurfaceHolder,
                format: Int,
                width: Int,
                height: Int
            ) {
                print("a")
            }

            override fun surfaceDestroyed(holder: SurfaceHolder) {
                cameraSource.release()
                bar.clearAnimation()
            }
        })

        startScan()
    }

    private fun startScan() {
        try {
            if (!barcodeDetector.isOperational) {
                Log.d("TAG", "Detector dependencies not loaded yet")
            } else {
                barcodeDetector.setProcessor(object : Detector.Processor<Barcode> {
                    override fun release() {
                        bar.clearAnimation()
                    }

                    override fun receiveDetections(detections: Detector.Detections<Barcode>) {
                        val barCodes = detections.detectedItems
                        if (barCodes.size() != 0) {
                            Log.d("TAG", "receiveDetections: ${barCodes.valueAt(0).displayValue}")
                            runOnUiThread {
                                Toast.makeText(
                                    c,
                                    barCodes.valueAt(0).displayValue, Toast.LENGTH_SHORT
                                ).show()
                            }
                            barcodeData = barCodes.valueAt(0).displayValue

                            val intent = Intent(
                                c,
                                MainActivity::class.java
                            )
                            intent.putExtra("barcodeData", barcodeData)
                            intent.putExtra("ScanBarcode", clickData)
                            barcodeDetector.release()
                            startActivity(intent)
                            runOnUiThread {
                                cameraSource.stop()
                            }
                            finish()
                        }
                    }
                })
            }
        } catch (e: Exception) {
            Log.d("TAG", "$e")
        }

    }


    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode != 1001) {
            Log.d("TAG", "Got unexpected permission result: $requestCode")
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            return
        }

        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            try {
                cameraSource.start(surfaceView.holder)
                bar.startAnimation(animation)
            } catch (e: Exception) {
                Log.d("CameraTAG", e.toString())
            }
        }
    }

    override fun onPause() {
        super.onPause()
        try {
            cameraSource.stop()
        } catch (e: Exception) {
        }
    }

}