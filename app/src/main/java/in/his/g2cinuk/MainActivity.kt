package `in`.his.g2cinuk

import `in`.his.g2cinuk.apiinterface.ApiService
import `in`.his.g2cinuk.apiinterface.RetroClient
import `in`.his.g2cinuk.dataModel.*
import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.media.AudioManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.speech.tts.TextToSpeech
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.barcode_scan.*
import kotlinx.android.synthetic.main.dialog.*
import kotlinx.android.synthetic.main.insert_product.*
import kotlinx.android.synthetic.main.insert_production.*
import kotlinx.android.synthetic.main.scan_product.*
import kotlinx.android.synthetic.main.search.*
import kotlinx.android.synthetic.main.select_line.*
import kotlinx.android.synthetic.main.spinner_dialog_block.*
import kotlinx.android.synthetic.main.spinner_dialog_sku.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity(), TextToSpeech.OnInitListener {
    private lateinit var insertNewDialog: Dialog
    private lateinit var scanProduct: Dialog
    private lateinit var insertProduct: Dialog
    private lateinit var lineSelectionDialog: Dialog
    private lateinit var skuDialog: Dialog
    private lateinit var blockDialog: Dialog
    private var selectedLine = ""
    private var selectedSku = ""
    private var selectedBatch = ""
    private var currentSkuPalletQty = 0.0
    private var totalLineProduction = 0
    var searchSpinnerItem = ""
    var keySearch = ""
    lateinit var searchAdapter: SearchAdapter
    private var lineSelected = false
    private var tts1: TextToSpeech? = null
    private var apiService: ApiService = RetroClient.getClient().create(ApiService::class.java)
    private var mAudioManager: AudioManager? = null
    lateinit var mqtt: MQtt
    private lateinit var viewPager: ViewPager
    private lateinit var clickObjHandle: ClickObjHandle
    var insertStatus = false
    lateinit var loginDialog: Dialog
    var pathBay = ""
    var selectedBay = ""
    var bwb: BarcodeListItem? = BarcodeListItem()
    var pd = ProductionDetailsItem()
    private var barcode: String? = ""
    private var cal = Calendar.getInstance()
    private var expiryDate = ""
    var clickData: String? = ""
    val dateFormatServer = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
    val sdf = SimpleDateFormat("dd MMMM yyyy", Locale.ENGLISH)
    var ipExpiryDate = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        clickObjHandle = ClickObjHandle.instance()
        clickObjHandle.addClick(object : HandleClick {
            override fun clickBay(bay: String, status: Boolean) {
                selectedBay = bay

                if (!insertStatus) {
                    val list = Search().searchFromBay(bay)
                    searchAdapter.setList(list)
                    tts(list)
                    Toast.makeText(this@MainActivity, bay, Toast.LENGTH_SHORT).show()
                } else {
                    if (!status) {
                        if (bay.contains("PATH")) {
                            showPassScreenForPath(bay)
                        } else {
                            startActivity(
                                Intent(this@MainActivity, BarcodeActivity::class.java)
                                    .putExtra("ScanBarcode", "ScanBay")
                            )
                            finish()
//                            showInsertDialog(bay)
                        }
                    } else {

                        Toast.makeText(
                            this@MainActivity,
                            "This bay is full,please select another",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                }
            }
        })

        scanProduct = Dialog(this)
        with(scanProduct) {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(R.layout.scan_product)
            window!!.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            window!!.attributes.windowAnimations = R.style.DialogTheme
            setCanceledOnTouchOutside(false)
            setCancelable(false)


            scan_product.setOnClickListener {
                startActivity(
                    Intent(
                        this@MainActivity,
                        BarcodeActivity::class.java
                    ).putExtra("ScanBarcode", "ScanProduct")
                )
                finish()
            }

            scan_product_barcode.setOnClickListener {
                startActivity(
                    Intent(
                        this@MainActivity,
                        BarcodeActivity::class.java
                    ).putExtra("ScanBarcode", "ScanBay")
                )
                finish()
            }

            scan_cancel.setOnClickListener {
                dismiss()
            }
        }

        insertProduct = Dialog(this)
        with(insertProduct) {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(R.layout.insert_product)
            window!!.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            window!!.attributes.windowAnimations = R.style.DialogTheme
            setCanceledOnTouchOutside(false)
            setCancelable(false)


            submit_new_product.setOnClickListener {
                if (ip_current_sku.text.isNotEmpty() && ip_expiry_date.text.isNotEmpty()
                ) {
                    submitProduct(
                        ip_current_sku.text.toString(),
                        ipExpiryDate,
                        ip_weight.text.toString(),
                        "1"
                    )
                }
            }

            cancel_new_product.setOnClickListener {
                insertProduct.dismiss()
                ip_current_qty.setText("")
                ip_current_sku.setText("")
                ip_weight.setText("")
                ipExpiryDate = ""
            }

            ip_calendar.setOnClickListener {
                val dateSetListener =
                    DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                        cal.set(Calendar.YEAR, year)
                        cal.set(Calendar.MONTH, monthOfYear)
                        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                        insertProduct.ip_expiry_date.text = sdf.format(cal.time)
                        ipExpiryDate = dateFormatServer.format(cal.time)
                    }
                val a =
                    DatePickerDialog(
                        this@MainActivity,
                        dateSetListener,
                        // set DatePickerDialog to point to today's date when it loads up
                        cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH)
                    )
                a.datePicker.minDate = System.currentTimeMillis()
                a.show()
            }

            insert_product_barcode.setOnClickListener {
                startActivity(
                    Intent(
                        this@MainActivity,
                        BarcodeActivity::class.java
                    ).putExtra("ScanBarcode", "Np")
                )
                finish()
            }

        }

        barcode = intent.extras?.get("barcodeData")?.toString()

        clickData = intent.extras?.get("ScanBarcode")?.toString()
        if (barcode != null) {
//            showInsertDialog(bay)
            when (clickData) {
                "ScanProduct" -> {
                    getProductDetails(barcode!!)

                }
                "ScanBay" -> {
                    getBarcodeWithBay(barcode!!)
                }
                "Np" -> {
                    getProductDetails(barcode!!)
                }
            }
        } else {
            if (clickData != null) {
                Toast.makeText(this, "Barcode Not Scanned Properly,Scan Again", Toast.LENGTH_SHORT)
                    .show()
            }
        }


        loginDialog = Dialog(this)
        with(loginDialog) {
            setContentView(R.layout.activity_login)
            setCancelable(true)
            setCanceledOnTouchOutside(true)


            login_tv_login.setOnClickListener {
                checkPassForPath(login_et_password.text.toString())
            }
        }

        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        viewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tab_layout)
        tabs.setupWithViewPager(viewPager)

        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))

        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                ViewListener.instance().changeView()
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                ViewListener.instance().changeView()
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                ViewListener.instance().changeView()
            }

        })


        //Initialize TTS
        tts1 = TextToSpeech(this, this)
        tts1!!.language = Locale.ENGLISH
        tts1!!.setSpeechRate(.70.toFloat())
        mqtt = MQtt.managerInstance

        mqtt.mQTTConnect(this, resources.getString(R.string.app_name))

        mqttCallings()

        mAudioManager = getSystemService(AUDIO_SERVICE) as AudioManager
        setDialogs()
        buttonClicks()

        try {
            searchAdapter = SearchAdapter()
            val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            rv_show_data_selected_bay.layoutManager = linearLayoutManager
            rv_show_data_selected_bay.adapter = searchAdapter
        } catch (e: Exception) {
        }

        pullToRefresh.setOnRefreshListener {
            try {
                pullToRefresh.isRefreshing = false
                Utils().getAllData()
//                Utils().getPlan(this)
                ViewListener.instance().changeView()
//                findEmptyBay()
            } catch (e: Exception) {
            }
        }

    }

    private fun submitProduct(sku: String, expiry: String, weight: String?, qty: String) {
        val call =
            apiService.SKU_ITEM_CALL(SkuItem(null, weight, expiry, null, sku, qty, Utils.pBarcode))

        call.enqueue(object : Callback<Password> {
            override fun onResponse(call: Call<Password>, response: Response<Password>) {
                if (response.isSuccessful && response.body() != null) {
                    if (response.body()!!.message == "Successful") {
                        if (clickData == "ScanProduct") {
                            insertManualProduction(
                                "1",
                                insertNewDialog.current_sku.text.toString()
                            )
                        }
                    }
                    if (response.body()!!.message == "Successful") {
                        Toast.makeText(
                            this@MainActivity,
                            "${response.body()!!.message} Added",
                            Toast.LENGTH_SHORT
                        ).show()
                        insertProduct.dismiss()
                    } else {
                        /*Toast.makeText(
                            this@MainActivity,
                            "${response.body()!!.message} Added",
                            Toast.LENGTH_SHORT
                        ).show()*/
                        insertProduct.dismiss()
                    }
                } else {
                    Toast.makeText(
                        this@MainActivity,
                        "Not Submitted , Try again",
                        Toast.LENGTH_SHORT
                    ).show()
                    insertProduct.dismiss()
                }
                insertProduct.ip_current_qty.setText("")
                insertProduct.ip_current_sku.setText("")
                insertProduct.ip_weight.setText("")
                ipExpiryDate = ""


            }

            override fun onFailure(call: Call<Password>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@MainActivity,
                        "Not Submitted , Try again",
                        Toast.LENGTH_SHORT
                    ).show()
                    insertProduct.dismiss()
                    insertProduct.ip_current_qty.setText("")
                    insertProduct.ip_current_sku.setText("")
                    insertProduct.ip_weight.setText("")
                    ipExpiryDate = ""
                } catch (e: Exception) {
                    print(e)
                }
            }

        })
    }

    private fun getBarcodeWithBay(barcode: String) {
        Utils.bayBarcode = barcode
        val call = apiService.BARCODE_WITH_BAY(barcode)
        call.enqueue(object : Callback<ResponseGetBarcodeWithBay?> {
            override fun onResponse(
                call: Call<ResponseGetBarcodeWithBay?>,
                response: Response<ResponseGetBarcodeWithBay?>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    when {
                        response.body()!!.barcodeList.isNullOrEmpty() -> {
                            Toast.makeText(
                                this@MainActivity,
                                "Barcode Not Found ",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        else -> {
                            bwb = response.body()!!.barcodeList?.get(0)
                            if (bwb != null) {
                                Utils.bayName = bwb!!.bay.toString()
                                scanProduct.show()
                                scanProduct.scan_current_bay_no.text = Utils.bayName
                                scanProduct.sp_barcode.text = Utils.bayBarcode
//                                insertProductionDialog.scan_current_bay_no.text = bwb!!.bay
                            }
                        }
                    }
                }
            }

            override fun onFailure(call: Call<ResponseGetBarcodeWithBay?>, t: Throwable) {
                Log.d("TAG", "onFailure: " + t.message)
                Toast.makeText(
                    this@MainActivity,
                    "Please Check Network Connection First",
                    Toast.LENGTH_SHORT
                ).show()
            }
        })
    }

    private fun getProductDetails(barcode: String) {
        Utils.pBarcode = barcode
        try {
            val call = apiService.PRODUCT_DETAILS(barcode)
            call.enqueue(object : Callback<ResponseAllSku?> {
                override fun onResponse(
                    call: Call<ResponseAllSku?>,
                    response: Response<ResponseAllSku?>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        when {
                            response.body()!!.skuData.isNullOrEmpty() -> {
                                when (clickData) {
                                    "Np" -> {
                                        insertProduct.show()
                                        insertProduct.ip_barcode.text = Utils.pBarcode
                                    }
                                    "ScanProduct" -> {
                                        insertNewDialog.show()
                                        insertNewDialog.current_barcode.text = Utils.pBarcode
                                        insertNewDialog.current_bay_no.text = Utils.bayName
                                        insertNewDialog.current_barcode.text = Utils.pBarcode
                                    }
                                    else -> {
                                        Toast.makeText(
                                            this@MainActivity,
                                            "Barcode Not Found",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                    }
                                }
                            }
                            else -> {
                                val pd = response.body()!!.skuData?.get(0)
                                if (pd != null) {
                                    expiryDate = pd.expiryDate.toString()
                                    if (clickData == "ScanProduct") {
                                        insertNewDialog.show()
                                        insertNewDialog.current_barcode.text = Utils.pBarcode
                                        insertNewDialog.current_bay_no.text = Utils.bayName
                                        insertNewDialog.current_barcode.text = Utils.pBarcode
                                        insertNewDialog.current_weight.setText(pd.palletWeight.toString())
                                        insertNewDialog.expiry_date.text =
                                            sdf.format(dateFormatServer.parse(expiryDate)!!)
                                        insertNewDialog.current_sku.setText(pd.sku)
                                        insertNewDialog.current_qty.setText(pd.casesOfPallets.toString())
                                    } else if (clickData == "Np") {
                                        insertProduct.show()
                                        insertProduct.ip_expiry_date.text =
                                            sdf.format(dateFormatServer.parse(expiryDate)!!)
                                        insertProduct.ip_current_sku.setText(pd.sku)
                                        insertProduct.ip_barcode.text = Utils.pBarcode
                                        insertProduct.ip_weight.setText(pd?.palletWeight?.toString())
                                    }
                                }
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<ResponseAllSku?>, t: Throwable) {
                    Log.d("TAG", "onFailure: " + t.message)
                    Toast.makeText(
                        this@MainActivity,
                        "Please Check Network Connection First",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
        } catch (e: Exception) {
            print(e)
        }
    }

    private fun insertManualProduction(qty: String, sku: String) {
        try {
            val call = apiService.INSERT_MANUAL_PRODUCTION(
                InsertManualProduction(
                    qty.toDouble().toInt(),
                    expiryDate,
                    sku,
                    Utils.bayBarcode,
                    Utils.pBarcode,
                    "pass"
                )
            )

            call.enqueue(object : Callback<ResponseInsertProduction> {
                override fun onResponse(
                    call: Call<ResponseInsertProduction>,
                    response: Response<ResponseInsertProduction>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            if (response.body()!!.message.isNullOrEmpty()) {
                                Toast.makeText(
                                    this@MainActivity,
                                    "Please verify again and resubmit",
                                    Toast.LENGTH_SHORT
                                )
                                    .show()

                                insertNewDialog.dismiss()


                            } else {
                                if (response.body()!!.message == "Successful") {
                                    Toast.makeText(
                                        this@MainActivity,
                                        "${response.body()!!.message}",
                                        Toast.LENGTH_SHORT
                                    )
                                        .show()

                                    insertNewDialog.dismiss()
                                    mqtt.publish("location", "DataChange".toByteArray())

                                } else {
                                    Toast.makeText(
                                        this@MainActivity,
                                        "${response.body()!!.message}",
                                        Toast.LENGTH_SHORT
                                    )
                                        .show()
                                    insertNewDialog.dismiss()

                                }
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<ResponseInsertProduction>, t: Throwable) {
                    Toast.makeText(
                        this@MainActivity,
                        "Please Check Your Connection and resubmit",
                        Toast.LENGTH_SHORT
                    ).show()
                    insertNewDialog.dismiss()

                }
            })

            /* } else {
                 Toast.makeText(
                     this,
                     "Please verify again and resubmit",
                     Toast.LENGTH_SHORT
                 )
                     .show()

             }*/
        } catch (e: Exception) {
            print(e)
        }
    }

    private fun displayDate() {
        val dateSetListener =
            DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateDateInView()
            }
        val a =
            DatePickerDialog(
                this,
                dateSetListener,
                // set DatePickerDialog to point to today's date when it loads up
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            )
        a.datePicker.minDate = System.currentTimeMillis()
        a.show()
    }

    private fun updateDateInView() {
        insertNewDialog.expiry_date.text = sdf.format(cal.time)
        expiryDate = dateFormatServer.format(cal.time)
    }

    private fun checkPassForPath(pass: String) {
        if (pass.isNotEmpty()) {
            val passwordCall = apiService.PATH_PASSWORD_CALL(pass)
            passwordCall.enqueue(object : Callback<Password?> {
                override fun onResponse(call: Call<Password?>, response: Response<Password?>) {
                    if (response.body() != null && response.body()!!.message != null) {
                        if ("Successfully" == response.body()!!.message) {
                            showInsertDialog(pathBay)
                            loginDialog.dismiss()
                        } else {
                            loginDialog.login_et_password.error = "Wrong Password"
                        }
                    } else {
                        Toast.makeText(
                            this@MainActivity,
                            "Please Check Network Connection First",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

                override fun onFailure(call: Call<Password?>, t: Throwable) {
                    Log.d("TAG", "onFailure: " + t.message)
                    Toast.makeText(
                        this@MainActivity,
                        "Please Check Network Connection First",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
        }
    }

    private fun showPassScreenForPath(bay: String) {
        loginDialog.show()
        pathBay = bay
        loginDialog.login_et_password.setText("")
    }

    private fun mqttCallings() {
        mqtt.addConnectionCalls(object : ConfirmConnection {
            override fun onSuccess() {
                try {
                    mqtt.subscribeTopic(this@MainActivity, "location")
                    mqtt.subscribeTopic(this@MainActivity, "status")
                    mqtt.subscribeTopic(this@MainActivity, "plan")
                } catch (e: Exception) {
                }
            }

            override fun onUnSuccess() {
                try {
                    Handler(Looper.getMainLooper()).postDelayed({
                        mqtt.mQTTConnect(
                            this@MainActivity,
                            resources.getString(R.string.app_name)
                        )
                    }, 30000)
                } catch (e: Exception) {
                    mqtt.mQTTConnect(
                        this@MainActivity,
                        resources.getString(R.string.app_name)
                    )
                }
            }
        })

        mqtt.setValueListener(object : MqttValues {
            override fun onStatus(status: String) {

            }

            override fun onLocation(location: String) {
//                mqtt.publish("location", "DataChange".toByteArray())
                try {
                    if (location == "DataChange") {
                        Utils().getAllData()
                        ViewListener.instance().changeView()
//                        findEmptyBay()
                    } else {
                        Utils().getAllData()
//                        findEmptyBay()
                    }
                } catch (e: Exception) {
                }
            }

            override fun onOthers(topic: String, string: String) {
                try {
                    /*if (topic == "plan")
                        Utils().getPlan(this@MainActivity)*/
                } catch (e: Exception) {
                }
            }

        })
    }

    private fun setDialogs() {
        insertNewDialog = Dialog(this)
        with(insertNewDialog) {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(R.layout.insert_production)
            window!!.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            window!!.attributes.windowAnimations = R.style.DialogTheme
            setCanceledOnTouchOutside(false)
            setCancelable(false)

            up_calendar.setOnClickListener {
                displayDate()
            }

            insert_production_barcode.setOnClickListener {
                startActivity(
                    Intent(
                        this@MainActivity,
                        BarcodeActivity::class.java
                    ).putExtra("ScanBarcode", "ScanProduct")
                )
                finish()
            }

            current_qty?.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {

                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    /*  try {
                          val remains =
                              Search().bayMaxCapacity(selectedBay)
                                  ?.minus(Search().getBayPalletQty(selectedBay))
                          val a = currentSkuPalletQty * remains!!

                          if (s?.isNotBlank() == true) {
                              if (current_qty.text.toString().toDouble() > a) {
                                  Toast.makeText(
                                      this@MainActivity,
                                      "Value exceeded bay capacity",
                                      Toast.LENGTH_SHORT
                                  ).show()
                                  insertNewDialog.submit_new_pallet.isEnabled = false
                              } else {
                                  insertNewDialog.submit_new_pallet.isEnabled = true
                              }
                          } else {
                              selectCurrentSkuPalletQty()
                              insertNewDialog.submit_new_pallet.isEnabled = true
                          }
                      } catch (e: Exception) {
                      }*/
                }

                override fun afterTextChanged(s: Editable?) {

                }
            })

            submit_new_pallet?.setOnClickListener {
                if (expiry_date.text.isEmpty() || current_sku.text.isEmpty()
                ) {
                    when {
                        current_sku.text.isEmpty() -> {
                            Toast.makeText(this@MainActivity, "Enter Sku", Toast.LENGTH_SHORT)
                                .show()
                        }
                        expiry_date.text.isEmpty() -> {
                            Toast.makeText(this@MainActivity, "Add Date", Toast.LENGTH_SHORT).show()
                        }
                    }
                } else {
                    submitProduct(
                        current_sku.text.toString(),
                        expiryDate,
                        current_weight.text.toString(),
                        "1"
                    )

                }

                /*if (lineSelected) {
                    if (current_qty.text.isNotEmpty()) {
                        try {
                            currentSkuPalletQty = current_qty.text.toString().toDouble()
                            submitNewPallet(current_bay_no.text.toString())

                        } catch (e: Exception) {

                        }
                    } else {
                        try {
                            submitNewPallet(current_bay_no.text.toString())
                            submit_new_pallet.isEnabled = false
                        } catch (e: Exception) {

                        }
                    }
                } else {
                    try {
                        currentSkuPalletQty = current_qty.text.toString().toDouble()
                        selectedSku = current_sku.text.toString()
                        selectedBatch = expiry_date.text.toString()
                        submitNewPallet(current_bay_no.text.toString())
                        submit_new_pallet.isEnabled = false
                    } catch (e: Exception) {
                    }
                }*/
            }

            cancel_new_pallet?.setOnClickListener {
                dismiss()
                expiry_date.text = ""
                current_sku.setText("")
            }

        }

        lineSelectionDialog = Dialog(this)
        with(lineSelectionDialog) {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(R.layout.select_line)
            window!!.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            window!!.attributes.windowAnimations = R.style.DialogTheme
            setCanceledOnTouchOutside(false)
            setCancelable(false)

            submit_line.setOnClickListener {
                try {
                    if (spinner_select_line.selectedItem != null) {
                        selectedLine = spinner_select_line.selectedItem?.toString()!!
                        Utils.proPlan.forEach {
                            if (selectedLine.split(":")[0].trim() == it?.lineNo
                                && selectedLine.split(":")[1].trim() == it.sku
                                && selectedLine.split(":")[2].trim() == it.batchNo
                            ) {
                                totalLineProduction = it.qty!!
                            }
                        }
                        dismiss()
                        selectCurrentSkuPalletQty()
                    }
                } catch (e: Exception) {
                }
            }

            cancel_line.setOnClickListener { dismiss() }

        }

        skuDialog = Dialog(this)
        with(skuDialog) {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(R.layout.spinner_dialog_sku)
            window!!.attributes.windowAnimations = R.style.DialogTheme
            setCanceledOnTouchOutside(true)
            setCancelable(true)

            try {
                val adapter: ArrayAdapter<String> = ArrayAdapter(
                    this@MainActivity,
                    android.R.layout.simple_list_item_1, android.R.id.text1, Utils.skusList
                )
                list_sku.adapter = adapter
                list_sku.onItemClickListener =
                    AdapterView.OnItemClickListener { _: AdapterView<*>?, _: View?, position: Int, _: Long ->
                        val skuItem = adapter.getItem(position)
                        Log.d("TAG", "init: $skuItem")
                        skuDialog.dismiss()
                        val list = Search().getSkuItem(skuItem)
                        searchAdapter.setList(list)
                        tts(list)
                        tv_search?.text = skuItem
                    }
            } catch (e: Exception) {
            }
        }

        blockDialog = Dialog(this)
        with(blockDialog) {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(R.layout.spinner_dialog_block)
            window!!.attributes.windowAnimations = R.style.DialogTheme
            setCanceledOnTouchOutside(true)
            setCancelable(true)

            block_a?.setOnClickListener {
                try {
                    val list = Search().searchFromBlock("A")
                    searchAdapter.setList(list)
                    tts(list)
                    tv_search?.setText(R.string.block_a)
                    dismiss()
                } catch (e: Exception) {
                }
            }
            block_B?.setOnClickListener {
                try {
                    val list = Search().searchFromBlock("B")
                    searchAdapter.setList(list)
                    tts(list)
                    tv_search?.setText(R.string.block__b)
                    dismiss()
                } catch (e: Exception) {
                }
            }
            block_C?.setOnClickListener {
                try {
                    val list = Search().searchFromBlock("C")
                    searchAdapter.setList(list)
                    tts(list)
                    tv_search?.setText(R.string.block__c)
                    dismiss()
                } catch (e: Exception) {
                }
            }
        }

        try {
            val searchAdapter = ArrayAdapter.createFromResource(
                this,
                R.array.search_spinner, R.layout.support_simple_spinner_dropdown_item
            )
            searchAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
            search_spinner?.adapter = searchAdapter
        } catch (e: Exception) {
        }
    }

    private fun selectAccLine() {
        lineSelected = true
        for (i in Utils.proPlan) {
            try {
                if (i!!.lineNo == selectedLine.split(":")[0].trim() &&
                    i.sku == selectedLine.split(":")[1].trim() &&
                    i.batchNo == selectedLine.split(":")[2].trim()
                ) {
                    selectedBatch = i.batchNo.toString()
                    selectedSku = i.sku
                }
            } catch (e: Exception) {
            }
        }

        for (i in Utils.skuList) {
            try {
                if (i!!.sku == selectedSku) {
                    currentSkuPalletQty = i.casesOfPallets!!.toDouble()
                }
            } catch (e: Exception) {
            }
        }
    }

    private fun selectCurrentSkuPalletQty() {
        lineSelected = true
        /* for (i in Utils.proPlan) {
             try {
                 if (i!!.lineNo == selectedLine.split(":")[0].trim() &&
                     i.sku == selectedLine.split(":")[1].trim() &&
                     i.batchNo == selectedLine.split(":")[2].trim()
                 ) {
                     selectedBatch = i.batchNo.toString()
                     selectedSku = i.sku
                 }
                 insert_new.text = resources.getString(R.string.insert)

             } catch (e: Exception) {
             }
         }*/
        currentSkuPalletQty = 0.0
        for (i in Utils.skuList) {
            try {
                if (i!!.sku == selectedSku) {
                    insertStatus = true
                    currentSkuPalletQty = i.casesOfPallets!!.toDouble()
                }
            } catch (e: Exception) {
            }
        }
        if (currentSkuPalletQty == 0.0) {
            Toast.makeText(
                this,
                "Connect to admin wrong SKU selected in plan",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun submitNewPallet(bayNo: String) {
        try {
            val insertModel = InsertModel(
                selectedSku, selectedBatch,
                currentSkuPalletQty.toInt().toString(), bayNo, "PASS"
            )
            val call =
                apiService.INSERT_RESPONSE_CALL(selectedLine.split(":")[0].trim(), insertModel)

            call.enqueue(object : Callback<Password> {
                override fun onFailure(call: Call<Password>, t: Throwable) {
                    Toast.makeText(
                        this@MainActivity,
                        "Please Check Network Connection First", Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onResponse(call: Call<Password>, response: Response<Password>) {
                    if (response.isSuccessful) {
                        when {
                            response.body()?.message.isNullOrBlank() -> {
                                Toast.makeText(
                                    this@MainActivity,
                                    "There is some Problem, Please Reconnect to Server",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                            else -> {
                                when (response.body()!!.message) {
                                    "Successful" -> {
                                        Toast.makeText(
                                            this@MainActivity,
                                            " " + response.body()!!.message,
                                            Toast.LENGTH_SHORT
                                        ).show()
//                                        Utils().getPlan(this@MainActivity)
                                        mqtt.publish("location", "DataChange".toByteArray())
                                    }
                                    "Limit Exceeded" -> {
                                        Toast.makeText(
                                            this@MainActivity,
                                            "Production Completed.Please Select Next Plan",
                                            Toast.LENGTH_SHORT
                                        ).show()
//                                        Utils().getPlan(this@MainActivity)
                                    }
                                    else -> {
                                        Toast.makeText(
                                            this@MainActivity,
                                            "There is some Problem, Please Try After Some Time ",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                    }
                                }
                            }
                        }
                    }
                }
            })
        } catch (e: Exception) {
        }
    }

    private fun buttonClicks() {
        insert_new?.setOnClickListener {
            if (insertStatus) {
                insertStatus = false
                insert_new?.text = resources.getString(R.string.search)
                selectedSku = ""
                selectedBatch = ""
                totalLineProduction = 0
            } else {
                insertStatus = true
                insert_new?.text = resources.getString(R.string.insert)
            }
        }

/*
        insertNewDialog.sku_list_spinner?.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {

                }

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    if (!lineSelected) {
                        insertNewDialog.current_sku.text =
                            insertNewDialog.sku_list_spinner.selectedItem.toString()
                    }
                }
            }*/

        more_options?.setOnClickListener {
            startActivity(
                Intent(
                    this@MainActivity,
                    BarcodeActivity::class.java
                ).putExtra("ScanBarcode", "Np")
            )
            finish()

//            showLine()
        }

        btl_fifo?.setOnClickListener {
            searchAdapter.sortFifo()

        }
        btl_lifo?.setOnClickListener {
            searchAdapter.sortLifo()
        }

        tts_on?.setOnClickListener {
            tts_off?.visibility = View.VISIBLE
            tts_on?.visibility = View.GONE
            try {
                if (tts1!!.isSpeaking) {
                    tts1!!.stop()
                    val fullSetVolume = 0
                    mAudioManager?.setStreamVolume(AudioManager.STREAM_MUSIC, fullSetVolume, 0)
                }
            } catch (e: java.lang.Exception) {
                Log.d("TAG", "onClick: $e")
            }
        }

        tts_off?.setOnClickListener {
            tts_off?.visibility = View.GONE
            tts_on?.visibility = View.VISIBLE
            try {
                val setVolume = 10
                mAudioManager?.setStreamVolume(AudioManager.STREAM_MUSIC, setVolume, 0)
            } catch (e: java.lang.Exception) {
                Log.d("TAG", "onClick: $e")
            }


        }

        et_search?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                keySearch = p0!!.toString()
                when (searchSpinnerItem) {
                    "Batch No" -> {
                        val list = Search().searchFromBatch(keySearch)
                        searchAdapter.setList(list)
                        tts(list)
                    }
                    "Bay No" -> {
                        val list = Search().searchFromBay(keySearch)
                        searchAdapter.setList(list)
                        tts(list)
                    }
                    "Select…" -> {
                        val list = Search().searchFromAll(keySearch)
                        searchAdapter.setList(list)
                        tts(list)
                    }
                }
            }
        })

        search_spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                searchSpinnerItem = search_spinner.selectedItem.toString()

                Log.d("TAG", "search_spinner$searchSpinnerItem")
                when (searchSpinnerItem) {
                    "Select…" -> {
                        et_search.isEnabled = true
                    }
                    "All" -> {
                        val list = Search().searchFromAllExceptEmpty("empty")
                        searchAdapter.setList(list)
                        tts(list)
                        tv_search.text = "All Items"
                        et_search.isEnabled = false
                    }
                    "SKU" -> {
                        et_search.isEnabled = false
                        skuDialog.show()
                    }
                    "Block" -> {
                        et_search.isEnabled = false
                        blockDialog.show()
                    }
                    "Bay No" -> {
                        et_search.isEnabled = true
                        searchSpinnerItem = search_spinner.selectedItem.toString()
                        tv_search.text = "From BAY"
                    }
                    "Batch No" -> {
                        et_search.isEnabled = true
                        searchSpinnerItem = search_spinner.selectedItem.toString()
                        tv_search.text = "From Expiry"
                    }
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    fun showInsertDialog(bay: String) {
        if (selectedSku.isNotBlank()) {
            insertNewDialog.show()
            selectCurrentSkuPalletQty()
            val skuAdapter = ArrayAdapter(
                this,
                R.layout.support_simple_spinner_dropdown_item,
                Utils.skusList
            )
            skuAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
            insertNewDialog.sku_list_spinner.adapter = skuAdapter

            /*       val bayAdapter = ArrayAdapter(
                       this,
                       R.layout.support_simple_spinner_dropdown_item, Utils.baysList
                   )
                   bayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)*/
//            insertNewDialog.bay_list_spinner.adapter = bayAdapter
            if (selectedLine.isNotEmpty()) {
//                insertNewDialog.current_sku.text = selectedSku
                /*      insertNewDialog.batch_no.setText(selectedBatch)
                      insertNewDialog.batch_no.isEnabled = false*/
                insertNewDialog.current_bay_no.text = bay
                insertNewDialog.current_bay_no.isEnabled = false
//                insertNewDialog.bay_list_spinner.isEnabled = false
                /* insertNewDialog.current_line_no.text =
                     getString(R.string.selected_line) + ": " +
                             selectedLine +
                             "  Total Production: " + totalLineProduction*/
                insertNewDialog.sku_list_spinner.isEnabled = false
            } else {
                /* insertNewDialog.current_line_no.text =
                     "if you want work acc. to plan please select line first"
                 insertNewDialog.batch_no.isEnabled = true*/
                insertNewDialog.sku_list_spinner.isEnabled = true
            }
        } else {
            showLine()
        }
    }

    private fun showLine() {
        try {
            lineSelectionDialog.show()
            val lineAdapter = ArrayAdapter(
                this,
                R.layout.support_simple_spinner_dropdown_item, Utils.lineNos
            )
            lineAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
            lineSelectionDialog.spinner_select_line.adapter = lineAdapter
        } catch (e: Exception) {
        }
    }

    fun tts(list: ArrayList<BayWithProductionItem>) {
        var speechText: String
        if (tts1!!.isSpeaking) {
            tts1!!.stop()
        }
        try {
            for (i in list.indices) {
                speechText = "bay " + java.lang.String.valueOf(list[i].bay) +
                        ",S.K.U " + java.lang.String.valueOf(list[i].sku) +
                        ",Expiry Date is " + java.lang.String.valueOf(list[i].expiry) +
                        ",Quantity " + java.lang.String.valueOf(list[i].qty)
                tts1!!.speak(speechText, TextToSpeech.QUEUE_ADD, null, "")
            }
        } catch (e: java.lang.Exception) {
            Log.e("TAG", "tts: ", e)
        }
    }

    override fun onInit(status: Int) {
        try {
            if (status == TextToSpeech.SUCCESS) {
                val result: Int = tts1?.setLanguage(Locale.US)!!
                if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED
                ) {
                    Log.d("textToSpeech", "This Language is not supported")
                } else {
                    Log.d("TAG", "onInit: success")
                }
            } else {
                Log.d("textToSpeech", "Initialization Failed!")
            }
        } catch (e: Exception) {
            Log.d("TAG", "onInit: $e")
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (tts1?.isSpeaking!!) {
            tts1?.stop()
            tts1?.shutdown()
        }
    }

    /* private fun findEmptyBay() {
         try {
             var list = Search().searchFromBay("FG-1")
             if (list[0].sku.equals("empty", true)) {
                 fg_1?.background = ResourcesCompat.getDrawable(resources, R.drawable.empty_back, null)
             } else {
                 fg_1?.background = ResourcesCompat.getDrawable(resources, R.drawable.main_back, null)
             }
             list.clear()
             list = Search().searchFromBay("FG-2")
             if (list[0].sku.equals("empty", true)) {
                 fg_2?.background = ResourcesCompat.getDrawable(resources, R.drawable.empty_back, null)
             } else {
                 fg_2?.background = ResourcesCompat.getDrawable(resources, R.drawable.main_back, null)
             }
             list.clear()
             list = Search().searchFromBay("FG-3")
             if (list[0].sku.equals("empty", true)) {
                 fg_3?.background = ResourcesCompat.getDrawable(resources, R.drawable.empty_back, null)
             } else {
                 fg_3?.background = ResourcesCompat.getDrawable(resources, R.drawable.main_back, null)
             }
             list.clear()
             list = Search().searchFromBay("FG-4")
             if (list[0].sku.equals("empty", true)) {
                 fg_4?.background = ResourcesCompat.getDrawable(resources, R.drawable.empty_back, null)
             } else {
                 fg_4?.background = ResourcesCompat.getDrawable(resources, R.drawable.main_back, null)
             }
             list.clear()
             list = Search().searchFromBay("FG-5")
             if (list[0].sku.equals("empty", true)) {
                 fg_5?.background = ResourcesCompat.getDrawable(resources, R.drawable.empty_back, null)
             } else {
                 fg_5?.background = ResourcesCompat.getDrawable(resources, R.drawable.main_back, null)
             }
             list.clear()
             list = Search().searchFromBay("FG-6")
             if (list[0].sku.equals("empty", true)) {
                 fg_6?.background = ResourcesCompat.getDrawable(resources, R.drawable.empty_back, null)
             } else {
                 fg_6?.background = ResourcesCompat.getDrawable(resources, R.drawable.main_back, null)
             }
             list.clear()
             list = Search().searchFromBay("FG-7")
             if (list[0].sku.equals("empty", true)) {
                 fg_7?.background = ResourcesCompat.getDrawable(resources, R.drawable.empty_back, null)
             } else {
                 fg_7?.background = ResourcesCompat.getDrawable(resources, R.drawable.main_back, null)
             }
             list.clear()
             list = Search().searchFromBay("FG-8")
             if (list[0].sku.equals("empty", true)) {
                 fg_8?.background = ResourcesCompat.getDrawable(resources, R.drawable.empty_back, null)
             } else {
                 fg_8?.background = ResourcesCompat.getDrawable(resources, R.drawable.main_back, null)
             }


         } catch (e: Exception) {

         }
     }*/
}