package `in`.his.g2cinuk

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter

private val TAB_TITLES = arrayOf(
    R.string.swinton_depot,
    R.string.heron_st_depot,
)

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).

        when (position) {

            0 -> {

                return SwintonDepot.newInstance()
            }
            1 -> {
              /*  return if (IssueFragment.newInstance().click) {
                    Log.d("click","1")*/
                    return HeronStDepot.newInstance()

                /*} else {
                    Log.d("click","0")
                    IssueFragment.newInstance()
                }*/
            }
           /* 2 -> {
              *//*  return if (IssueFragment.newInstance().click) {
                    Log.d("click","1")*//*
                    return SecondFloorFragment.newInstance()

                *//*} else {
                    Log.d("click","0")
                    IssueFragment.newInstance()
                }*//*
            }*/

        }

        return SwintonDepot.newInstance()
    }


    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        // Show 2 total pages.
        return 2
    }


}